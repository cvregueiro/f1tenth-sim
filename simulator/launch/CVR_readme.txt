# to execute

In terminal 1:
  roslaunch f1tenth-sim simulator.launch run_gazebo:=true keyboard_control:=true

In terminal 2: go to directory "src/simulator/rviz"
  rviz -d car_1.rviz 

In terminal 3:
  rosrun  f1tenth-sim command_multiplexer.py car_1 teleop

In terminal 4:
  rosrun f1tenth-sim keyboard_teleop.py car_1


